package com.sptpc.book.common;

public enum OrderState {
    INIT,PAID,BREWING,TAKEN,CANCELLED
}
