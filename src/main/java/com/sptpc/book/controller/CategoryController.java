package com.sptpc.book.controller;

import com.sptpc.book.controller.request.NewCategoryRequest;
import com.sptpc.book.controller.request.NewOrderRequest;
import com.sptpc.book.model.Book;
import com.sptpc.book.model.Category;
import com.sptpc.book.model.Order;
import com.sptpc.book.service.BookService;
import com.sptpc.book.service.CategoryService;
import com.sptpc.book.service.OrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private OrderService orderService;


    @GetMapping(value = "/login", params = "!name")
    public ModelAndView getAll() {
        ModelAndView mv = new ModelAndView("/user/index");
        List<Category> categoryList = categoryService.findAll();
        mv.addObject("category", categoryList);
        return mv;
    }
    


//    @GetMapping(value = "/", params = "name")
//    public Category getCategory(@RequestParam String name) {
//        return categoryService.findOne(name);
//    }
//
//    @GetMapping(value = "/{id}")
//    public Category getCategory(@RequestParam Long id){
//        return categoryService.findOne(id);
//    }
//
//
    @PostMapping(value = "/save",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseStatus(code = HttpStatus.CREATED)
    public Category saveCategory(NewCategoryRequest categoryRequest){
        Category category= new Category();
        BeanUtils.copyProperties(categoryRequest,category);

        return categoryService.saveCategory(category);
    }

}
