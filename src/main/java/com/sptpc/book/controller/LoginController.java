package com.sptpc.book.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/")
public class LoginController {
    @GetMapping(value = "/")
    public ModelAndView Login(){
        ModelAndView mv=new ModelAndView("/login");
        return mv;
    }
    @GetMapping(value = "/regist")
    public ModelAndView Regist(){
        ModelAndView mv=new ModelAndView("/regist");
        return mv;
    }

}
