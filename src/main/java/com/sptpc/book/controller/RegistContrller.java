package com.sptpc.book.controller;

import com.sptpc.book.controller.request.NewUserRequest;
import com.sptpc.book.model.User;
import com.sptpc.book.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@RequestMapping("/user")
@RestController
@Controller
public class RegistContrller {
    @Autowired
    private UserService userService;

    @PostMapping("/regist")
    public ModelAndView regist(HttpSession session, NewUserRequest userRequest) {
        ModelAndView mv = new ModelAndView("/login");
        User user = new User();
        BeanUtils.copyProperties(userRequest,user);
        session.removeAttribute("regMsg");
        userService.saveUser(user);
        session.setAttribute("regMsg","用户注册成功" );
        return mv;
    }

}
