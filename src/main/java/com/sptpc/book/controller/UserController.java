package com.sptpc.book.controller;

import com.sptpc.book.controller.request.NewUserRequest;
import com.sptpc.book.model.User;
import com.sptpc.book.repository.UserRepository;
import com.sptpc.book.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Controller
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/")
    public ModelAndView login  (String name, String password,
                                @RequestParam("user_type") String user_type,
                                HttpServletRequest request) {
        User user = userService.findOne(name, password);
        HttpSession session = request.getSession();
        if (null == user) {
            System.out.println("登陆失败");
            ModelAndView mv = new ModelAndView("/403");
            return mv;
        } else {
            System.out.println("登陆成功");
            if (user_type.equals("用户")) {
                if (user.getUser_type().equals("用户")) {
                    ModelAndView mv = new ModelAndView("/success");
                    session.setAttribute("user",name);
                    mv.addObject("user",name);
                    return mv;
                } else {
                    ModelAndView mv = new ModelAndView("/403");
                    return mv;
                }
            }
            if (user_type.equals("管理员")) {
                if (user.getUser_type().equals("管理员")) {
                    ModelAndView mv = new ModelAndView("/success");
                    session.setAttribute("user",name);
                    mv.addObject("user",name);
                    return mv;
                } else {
                    ModelAndView mv = new ModelAndView("/403");
                    return mv;
                }
            }
            ModelAndView mv = new ModelAndView("/403");
            return mv;
        }

    }
}
//    @GetMapping(value = "/")
//    public ModelAndView getUser(){
//        ModelAndView mv=new ModelAndView("/user/index");
//        List<User> userList =userService.findAll();
//        mv.addObject("",userList);
//        return mv;
//    }