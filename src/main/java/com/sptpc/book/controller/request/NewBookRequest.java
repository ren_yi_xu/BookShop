package com.sptpc.book.controller.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import org.joda.money.Money;

@Data
@AllArgsConstructor
public class NewBookRequest {
    private String name;
    private String author;
    private String number;
    @NonNull
    private Long categoryId;
    private Money price;
}
