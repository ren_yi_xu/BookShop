package com.sptpc.book.controller.request;

import lombok.Data;
@Data
public class NewCategoryRequest {
    private String name;
}
