package com.sptpc.book.controller.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewUserRequest {
    private String user_type;
    private String sex;
    private String name;
    private String password;
    private String email;
}
