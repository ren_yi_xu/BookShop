package com.sptpc.book.model;

import lombok.*;
import org.hibernate.annotations.Type;
import org.joda.money.Money;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "b_book")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Category category;
    private String name;
    private String author;
    private String number;
    @Type(type = "org.jadira.usertype.moneyandcurrency.joda.PersistentMoneyMinorAmount",
            parameters = {@org.hibernate.annotations.Parameter(name="currencyCode",value ="CNY")})
    private Money price;

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id + '\'' +
                "category=" + category +
                ", name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", number='" + number + '\'' +
                ", price=" + price +
                "} " + super.toString();
    }
}
