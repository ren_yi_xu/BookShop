package com.sptpc.book.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sptpc.book.common.OrderState;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "b_order")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonIgnoreProperties(value = { "hibernateLazyInitializer"})
    @ManyToMany
    @JoinTable(name = "b_order_book",joinColumns = {@JoinColumn(name = "book_order_id")})
    private List<Book> items;
    private String user;
    @Enumerated
    @Column(nullable = false)
    private OrderState state;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id + '\'' +
                ",items='" + items+ '\''+
                ",user='" + user+ '\'' +
                ", state=" + state +
                "} " ;
    }
}
