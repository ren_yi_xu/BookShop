package com.sptpc.book.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "b_user")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(updatable = false)
    @CreationTimestamp
    private LocalDateTime createTime;
    private String user_type;
    private String sex;
    private String name;
    private String password;
    private String email;


    @Override
    public String toString() {
        return "User{" +
                "id=" + id + '\'' +
                "user_type=" + user_type + '\'' +
                "sex=" + sex + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                "email=" + email + '\'' +
                "}"+ super.toString();
    }
}
