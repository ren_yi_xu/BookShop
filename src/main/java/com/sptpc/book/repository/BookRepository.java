package com.sptpc.book.repository;

import com.sptpc.book.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book,Long> {

    List<Book> getBooksByCategory_Id(Long categoryId);

    Optional<Book> findBookByNameLike(String name);
}
