package com.sptpc.book.repository;

import com.sptpc.book.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category,Long> {
    Optional<Category> findCategoriesByNameLike(String name);
}
