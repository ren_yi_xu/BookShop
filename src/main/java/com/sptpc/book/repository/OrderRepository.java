package com.sptpc.book.repository;

import com.sptpc.book.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order,Long> {

    List<Order> findOrderByUserLike(String user);

    Order findOrdersById(long id);
}
