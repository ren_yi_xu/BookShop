package com.sptpc.book.repository;

import com.sptpc.book.model.Category;
import com.sptpc.book.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

//@Mapper
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findUserByNameAndPassword(String name,String password);

//    @Select("select *from b_user where name = #{name} and password = #{password}")
//    public User login(@Param("name") String name, @Param("password") String password);
}
