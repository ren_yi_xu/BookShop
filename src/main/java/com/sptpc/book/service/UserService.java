package com.sptpc.book.service;

import com.sptpc.book.model.Book;
import com.sptpc.book.model.Category;
import com.sptpc.book.model.User;
import com.sptpc.book.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User findOne(String name,String password) {
        Optional<User> user = userRepository.findUserByNameAndPassword(name,password);
        if (user.isPresent()) {
            return user.get();
        } else {
            return null;
        }
    }
//
//    public User getUser(Long id) {
//        return userRepository.getOne(id);
//    }
//
//    public List<User> findAll() {
//        return userRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
//    }
}